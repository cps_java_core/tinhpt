import java.util.HashMap;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {

        Scanner scanner = new Scanner(System.in);
        HashMap<String, Double> currency = new HashMap<>();
        String standard = "VND";
        currency.put("VND", 1.0);
        currency.put("USD", 23.174);
        currency.put("EUR", 26.977);    
        
        String keyOrigin = dependencyStringWithLength(scanner,"Tiền gì?",currency);
        double valueOrgin = dependencyReadDouble(scanner,"Bao nhiêu?");
        String convert = dependencyStringWithLength(scanner,"Chuyển sang tiền gì?",currency);

        double result = 0.0;

        if(!convert.equals(standard)){
            double s = valueOrgin * currency.get(keyOrigin);
            if(currency.get(standard) < currency.get(convert)){
                result = s / currency.get(convert);
            }else{
                result = s * currency.get(convert);
            }
        }else{
            result = valueOrgin * currency.get(keyOrigin);
        }

        System.out.println("Ket qua \t" + result);

    }

    public static double dependencyReadDouble(Scanner scanner, String ask){
        while (true) {
            System.out.print(ask);
            String value = scanner.nextLine();
            try {
                double result = Double.parseDouble(value);
                if (result <= 0) {
                    continue;
                } else {
                    return result;
                }
            } catch (Exception e) {
                System.out.print("Type input formatter was error!");
            }
        }

    }

    public static String dependencyStringWithLength(Scanner scanner,String ask,HashMap<String, Double> currency) {
        while (true) {
            System.out.print(ask);
            String value = scanner.nextLine();
            if (!isAlloweCurrency(currency, value)){
                continue;
            } else {
                return value;
            }
        }
    }

    public static boolean isAlloweCurrency(HashMap<String, Double> currency, String check){
        
        for (String key : currency.keySet()) {
            if(key == check){
                return true;
            }
        }
        return false;

    }

}
