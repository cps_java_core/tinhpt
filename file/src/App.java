import java.io.*;
import java.util.*;

public class App {
    public static void main(String[] args) throws Exception {
        // writeFile();
        // outStream(); 
        // inputStream();
        readFile();
    }

    /**
     * Luồng character
     *          FileReader -> đọc file
     *          FileWriter -> ghi file
     * ==> làm việc chủ yếu với dạng string
     */
    public static void writeFile(){
        try {
            File f = new File("./resource/write.txt");
            FileWriter fw = new FileWriter(f);
            // fw.write("Ghi dữ liệu bằng luồng character");
            fw.write("9.5");;
            //Bước 3: Đóng luồng
            fw.close();
          } catch (IOException ex) {
            System.out.println("Loi ghi file: " + ex);
        }
    }

    public static void readFile(){
        try {
            File f = new File("./resource/data.txt");
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            String line;
            ArrayList<String> ds = new ArrayList<>();
            while ((line = br.readLine()) != null){
            ds.add(line);
            line = line.replaceAll("_", " ");
            }

            fr.close();
            br.close();

           } catch (Exception ex) {
             System.out.println("Loi doc file: "+ex);
         }

    }


    /**
     * Luồng byte 
     *          DataOutputStream -> ghi file
     *          DataIntputStream -> đọc
     * ==> Làm việc chủ yếu với dạng nhị phân
     * ==> Thông thường làm việc với gửi dữ liệu qua mạng (internet, bluetooth...)
     */
    public static void outStream(){
        try {
            
            FileOutputStream fos = new FileOutputStream("./resource/write.txt");
            DataOutputStream dos = new DataOutputStream(fos);
            ArrayList<Student> studens = new ArrayList<>();
            for (Student student : studens) {
                dos.writeUTF(student.toString());
            }
            fos.close();
            dos.close();
            System.out.println("Done!");
           } catch (IOException ex) {
             ex.printStackTrace();
           } 
    }

    public static void inputStream(){
        try {
            // File f = new File("./resource/data.txt");
            // FileReader fr = new FileReader(f);
            // BufferedReader br = new BufferedReader(fr);
            FileInputStream fis = new FileInputStream("./resource/write.txt");
            DataInputStream dis = new DataInputStream(fis);
            String str;
            while((str = dis.readUTF()) !=null){

            }
            
            fis.close();
            dis.close();
            System.out.println(str);
           } catch (IOException ex) {
             ex.printStackTrace();
           } 
    }
}
