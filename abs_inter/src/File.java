import java.util.ArrayList;

public abstract class File {

    public String name;
    public boolean isFolder;
    private ArrayList<File> children;
    public String extension;
    public FileType type;

    public File(){

    }

    public File(String name, String extension, FileType type){
        this.name = name;
        this.extension = extension;
        this.type = type;
    }

    
    public File initFolder(){
        this.isFolder = true;
        this.children = new ArrayList<>();
        return this;
    }

    public void addFile(File file){
        if(this.isFolder){
            children.add(file);
        }
    }

    /**
     * 
     * @return
     */
    public ArrayList<File> getChild(){
        return this.children;
    }

    /**
     * abstract method 
     * không có thân hàm
     */
    public abstract void open();


}

enum FileType {
    media, document, execute
    /**
     * media: 
     *      - images:  
     *               - png
     *               - jpg 
     *      - music
     *              - mp3
     *              - m4a
     *      - video
     *              - mp4
     * document:
     *      - notepad
     *      - word
     * 
     * execute:
     *      - exe
     *      - bat
     */
}