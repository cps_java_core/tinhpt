import java.util.ArrayList;



public class RednerNotification {
    
    ArrayList<Notification> notifications;
    
    public RednerNotification(){
        this.notifications = new ArrayList<>();
        this.notifications.add(new LikeNotification());
        this.notifications.add(new TagNotification());
        this.notifications.add(new LikeNotification());
    }



    public void render(){
        for (Notification notification : notifications) {
            notification.build();
        }
    }

}
