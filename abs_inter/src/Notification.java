public abstract class Notification {
    
    public abstract String setAvatar();
    public abstract String setContent();

    public void build(){
        System.out.println(setAvatar() + setContent());
    }

}
