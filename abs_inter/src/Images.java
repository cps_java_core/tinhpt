import java.util.ArrayList;

public class Images extends File {

    public int width;
    public int height;

    public Images(int width, int height, String name, String extension, FileType type) {
        super(name, extension, type);
        this.width = width;
        this.height = height;
    }

    @Override
    public void open() {
        System.out.println("Media");
    }

    @Override
    public ArrayList<File> getChild() {
            // do something
        return super.getChild();
    }

}
