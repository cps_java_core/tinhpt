public class App {
    public static void main(String[] args) throws Exception {
        RednerNotification notifications = new RednerNotification();
        notifications.render();
    }



    /**
     * Mô tả một lớp File là abstract
     * Mô tả một Interface định nghĩa file đó hệ thống có mở được hay không hoặc mở rộng ...
     * Chương trình quản lý File
     */


      /**
       * Abstract || class 
       * extends => kế thừa 
     * Trừu tượng
     * ==> Lớp không hoàn chỉnh ==> Các lớp con có thể kế thừa và bổ sung
     * -- phương thức - method 
     *               --- abstract 
     *               --- none abstract
     * -- attributes 
     * Có thể khai báo final || static cho att || metthod hay không? 
     * -- cho phép khai báo biến final
     * -- nếu là phương thức abstract thì bắt buộc override và ngược lại thì không
     * -- một lớp kế thừa từ ABS class || class thì kế thừa luôn cả các thuộc tính  -> tính chất hướng đối tượng
     * 
     */ 

     /**
      * Interface 
      implement => thực thi
      -- Trừu tượng 
      -- Tạo ra những thứ mở rộng hoặc bắt buộc
      Listenner củng là chức năng mà phải Interface
      -- Cách thức tạo ra một Listenner || Callback trong Java
      -- 1 phương thức được thực thi thì nơi nào thực thi interface đó sẽ thực thi phương thức đó
       class A impl Z (ak()) class B impl Z (ak())
       a.ak() -> b.ak();==> listener ==> callback ?

       -- Interface chỉ có mỗi các phương thức là abstract và chỉ là phương thức abstract 
       -- phương thức abstract => không có thân hàm!
      */

      /**
       * Khi làm việc với nhiều thực thể có thể quy về một thực thể đại diện <==> trừu tượng và đa hình
       * Nhắc tới Interface || Abstract là trừu tượng và đa hình 
       * Nhưng nhắc tới trừu tượng  + đa hình thì không nhất thiết phải cùng tồn tại cả Interface và Abstract
       * Abstract là linh hồn 
       * Interface là hợp đồng 
       * 1 class chỉ có một linh hồn nhưng có nhiều hợp đồng. 
       * nghĩa là 1 class chỉ đc kế thừa từ 1 lớp abstract hoặc class và thực thi đc cùng lúc nhiều Interface.
       */


       /**
        * ĐA KẾ THỪA  
        khác hoàn toàn với việc thực thi nhiều Interface
        class A extends B,C,D implement Z
        B != C != D => đa kế thừa Oke 
        B,C,D => render();
        class B extends D ?
        class C extends D ?
        class A extends B,C,D
        render() ? B,C,D?
        */

        /**
         * DART 
         * mixin => có thể bị đa kế thừa 
         * key: with 
         * mixin A để mở rộng cho các Notification
         * mixin A on Notification => tránh đa kế thừa
         * 
         */

}
