import java.util.ArrayList;
import java.util.Scanner;

public class Feculity {

    ArrayList<Student> students; // collection -> araylist, hashmap, linkedlist
    String a; // kieu du lieu nguyen thuy. -> array
    int[] ab = new int[100]; // array
    //constructor
    public Feculity(){
        this.students = new ArrayList<>();
    } 

    //method
    public Student initStudentFromKeyboard(Scanner scanner){
        System.out.print("Nhap vao ID: ");
        String id = scanner.nextLine();
        System.out.print("Nhap vao ho ten: ");
        String fN = scanner.nextLine();
        System.out.print("Nhap vao phone: ");
        String phone = scanner.nextLine();
        System.out.print("Nhap vao address: ");
        String adr = scanner.nextLine();
        System.out.print("Nhap vao tuoi: ");
        int age = scanner.nextInt();
        scanner.nextLine();
        return new Student(id,fN,age,adr,phone);
    }

    public void loopInitStudent(){
        Scanner scanner = new Scanner(System.in);
        while(true){
            System.out.println("An mot phim bat ki + Enter de nhap vao thong tin mot Sinh Vien. Nhap Exit de thoat nhap");
            String flag = scanner.nextLine();
            if(flag.toLowerCase().equals("exit")){
                break;
            }
            Student s = initStudentFromKeyboard(scanner);
            this.students.add(s);
        }
        scanner.close();
    }

    public void displayStudent(Student student){
        System.out.println(student.stringOfValue());
    }

    public void displayAll(){
        for (Student student : students) {
            displayStudent(student);
        }
    }

    public int getSize(){
        return this.students.size();
    }

    public Student search(String id){

        // for (Student student : students) {
        //     if(student.id.equals(id)){
        //         return student;
        //     }
        // }

        // return null;
        return this.students.stream().filter((s) ->s.id.equals(id)).findFirst().orElse(null);
    }

    public ArrayList<Student> searchByName(String name){
        ArrayList<Student> result = new ArrayList<>();
          for (Student student : students) {
            if(student.fullName.equals(name)){
                result.add(student);
            }
        }
        return result;
        }

    public void initFake(){
        this.students.add(new Student("1610227","Phan Trung Tinh",22,"Binh Dinh","0352974899"));
        this.students.add(new Student("1610191","Nguyen Thanh Quoc",22,"Lam Dong","089765432"));
    }


     
}
