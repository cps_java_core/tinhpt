import java.util.HashMap;

public class Student {
    public String id;
    public String fullName;
    public int age;
    public String address;
    public String phone;

    
    public Student() {

    }

    public Student(String id, String fullName, int age, String address, String phone) {
        this.id = id;
        this.fullName = fullName;
        this.age = age;
        this.address = address;
        this.phone = phone;
    }

    public Student(HashMap<String, String> json) {
        this.id = json.get("id");
        this.address = json.get("address");
        this.fullName = json.get("full_name");
        this.phone = json.get("phone");
        this.age = Integer.parseInt(json.get("age"));
    }

    public HashMap<String, String> toJson() {
        HashMap<String, String> json = new HashMap<>();
        json.put("id", this.id);
        json.put("full_name", this.fullName);
        json.put("age", String.valueOf(this.age));
        json.put("address", this.address);
        json.put("phone", this.phone);
        return json;
    }

    public String stringOfValue() {
        String out = " ";
        HashMap<String, String> interiable = this.toJson();
        for (String key : interiable.keySet()) {
            out += key +": "+  interiable.get(key) + "\t";
        }
        return out.trim();
    }
}
