import java.util.Scanner;

public class App {

    public static void main(String[] args) throws Exception {

        boolean  result = isUniqueChar("abcd");
        if(result){
            System.out.println("none");
        }else{
            System.out.println("has");
        }

    }

    public static boolean isUniqueChar(String str) {
        if (str.length() > 128) {
            return false;
        }
        boolean[] char_set = new boolean[128];
        for (int i = 0; i < str.length(); i++) {
            // char vs int? 
            int val = str.charAt(i);
            if (char_set[val]) {
                return false;
            }
            char_set[val] = true;
        }
        return true;
    }

}
